export class CreateBranchDto {
  id: number;
  address: string;
  status: 'open' | 'close';
}
